class Player

  attr_accessor :name, :attack_power, :defence, :health, :mana, :strength, :agility, :dodge

  def name
    @name || generateRandomName
  end

  def attack_power
    @attack_power || 1
  end

  def defence
    @defence || 1
  end

  def health
    @health || 100
  end

  def mana
    @mana || 20
  end

  def strength
    @strength || 1
  end

  def agility
    @agility || 1
  end

  def dodge
    @dodge || 1
  end

  def attributes
    instance_variables.map do |var|
      [var[1..-1].to_sym, instance_variable_get(var)]
    end.to_h
  end

  private

  def generateRandomName
    # Keeping this very simple.
    @name = ["Jamin", "Luce", "Jevon", "Fredy", "Hamilinis", "Emmano", "Shamarcul", "Gagaedric", "Jary", "Raelis"].sample
  end

end
