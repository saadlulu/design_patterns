require File.expand_path('player.rb')
class Mage < Player

  ExtraHealth = -20
  ExtraMana   = 10
  PostfixName = "_the_mage"

  def initialize
    self.health += ExtraHealth
    self.name += PostfixName
    self.mana += ExtraMana
  end

  def executeSpecialMove
    meditate
  end

  private

  def meditate
    self.mana += 5
  end
end
