require File.expand_path('player.rb')
class Warrior < Player

  ExtraHealth = 10
  ExtraMana   = -10
  PostfixName = "_the_warrior"

  def initialize
    self.health += ExtraHealth
    self.name += PostfixName
    self.mana += ExtraMana
  end

  def executeSpecialMove
    flex
  end

  private

  def flex
    # Keeping it extra simple.
    if health > 0
      self.health += 10
      puts "health regenerated, current health is: #{self.health}"
    end

  end

end
